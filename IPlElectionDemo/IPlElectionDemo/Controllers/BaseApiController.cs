﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using IPLElectionDemo.Entities.Models.HelperModel;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace IPlElectionDemo.Work.Controllers
{
    public class BaseApiController : ControllerBase
    {
        public BaseApiController()
        {

        }
        /// <summary>
        /// Send back HttpStatusCode.BadRequest along with a message.
        /// We are not using the default BadRequest() that is there in ControllerBase. This is due to keep our response object consistent
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns>An instance of ApiError with StatusCode 400</returns>
        internal BadRequestObjectResult BadRequest(string message)
        {
            return base.BadRequest(new ApiError
            {
                Status = (short)HttpStatusCode.BadRequest,
                Message = message
            });
        }
        /// <summary>
        /// Send back HttpStatusCode.BadRequest a message and List of Errors.
        /// We are not using the default BadRequest() that is there in ControllerBase. This is due to keep our response object consistent
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="errors">The errors.</param>
        /// <returns>An instance of ApiError with StatusCode 400</returns>
        internal BadRequestObjectResult BadRequest(string message, IEnumerable<string> errors)
        {
            return base.BadRequest(new ApiError
            {
                Status = (short)HttpStatusCode.BadRequest,
                Message = message,
                Errors = errors
            });
        }
        /// <summary>
        /// Send back HttpStatusCode.BadRequest along with a message and some additional data.
        /// We are not using the default BadRequest() that is there in ControllerBase. This is due to keep our response object consistent
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="errors">The errors.</param>
        /// <param name="additionalData">The additional data.</param>
        /// <returns>An instance of ApiError with StatusCode 400</returns>
        internal BadRequestObjectResult BadRequest(string message, IEnumerable<string> errors, object additionalData)
        {
            return base.BadRequest(new ApiError
            {
                Status = (short)HttpStatusCode.BadRequest,
                Message = message,
                Errors = errors,
                Data = additionalData
            });
        }

        /// <summary>
        /// Send back HttpStatusCode.BadRequest a message and additional data.
        /// We are not using the default BadRequest() that is there in ControllerBase. This is due to keep our response object consistent
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="additionalData">The additionalData data.</param>
        /// <returns>An instance of ApiError with StatusCode 400</returns>
        internal BadRequestObjectResult BadRequest(string message, object additionalData)
        {
            return base.BadRequest(new ApiError
            {
                Status = (short)HttpStatusCode.BadRequest,
                Message = message,
                Data = additionalData
            });
        }
        /// <summary>
        /// Send back HttpStatusCode.BadRequest
        /// </summary>
        /// <returns>An instance of ApiError with StatusCode 400</returns>
        internal new BadRequestObjectResult BadRequest()
        {
            return base.BadRequest(new ApiError
            {
                Status = (short)HttpStatusCode.BadRequest
            });
        }  
    }
}
