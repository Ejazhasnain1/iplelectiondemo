﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace IPLElectionDemo.Middleware
{
    public static class ServiceExtensibleCollection
    {
        public static IServiceCollection AppDependencies(this IServiceCollection services)
        {
            return services;
        }
        public static IMvcBuilder AddCustomMvc(this IServiceCollection services)
        {
            return services
                    .AddCors(option => option.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        //.AllowCredentials()
                        .AllowAnyOrigin();
                    }))
                    .Configure<MvcOptions>(
                                options => { options.Filters.Add(new CorsAuthorizationFilterFactory("AllowAll")); })
                    .AddMvc()
                    //Though Core2.2 sends back CamelCases on response, it does not work the same way for Model ValidationResults.
                    .AddJsonOptions(options =>
                                options.SerializerSettings.ContractResolver =
                                            new CamelCasePropertyNamesContractResolver())
                    .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

    }
}
