﻿using IPLElectionDemo.Entities.Models.DBModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace IPLElectionDemo.Entities.Models
{
    public partial class IPLElectionDbContext : DbContext
    {
        public IPLElectionDbContext(DbContextOptions<IPLElectionDbContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<UserRoles>().HasData(
                new { RoleId = 1, RoleName = "Admin" },
                new { RoleId = 2, RoleName = "Users" },
                new { RoleId = 3, RoleName = "Players" }
            );

            builder.Entity<IPLTeams>().HasData(
                new { TeamId = 1, TeamName = "Chennai Super Kings" },
                new { TeamId = 2, TeamName = "Kolkata Knight Riders" },
                new { TeamId = 3, TeamName = "Rajasthan Royals" },
                new { TeamId = 4, TeamName = "Mumbai Indians" },
                new { TeamId = 5, TeamName = "Delhi Daredevils" },
                new { TeamId = 6, TeamName = "Kings X1 Punjab" },
                new { TeamId = 7, TeamName = "Sunrisers Hyderabad" },
                new { TeamId = 8, TeamName = "Royal Challengers Banglore" }
            );
        }
        public DbSet<Users> users { get; set; }
        public DbSet<UserVoteDetails> userVoteDetails { get; set; }
        public DbSet<VoteResult> voteResult { get; set; }

    }
}
