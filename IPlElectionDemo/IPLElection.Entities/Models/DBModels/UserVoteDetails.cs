﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IPLElectionDemo.Entities.Models.DBModels
{
    public class UserVoteDetails
    {
        [Key]
        public int VoteId { get; set; }
        [ForeignKey("UserDetails")]
        public int UserID { get; set; }
        public Users UserDetails { get; set; }
        public bool Vote { get; set; }
        [ForeignKey("TeamDetails")]
        public int TeamId { get; set; }
        public IPLTeams TeamDetails { get; set; }
    }
}
