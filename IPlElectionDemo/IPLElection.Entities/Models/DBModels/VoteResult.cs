﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IPLElectionDemo.Entities.Models.DBModels
{
    public class VoteResult
    {
        [Key]
        public int ResultId { get; set; }
        public int? TotalVote { get; set; }
        [ForeignKey("TeamResultTotal")]
        public int TeamId { get; set; }
        public IPLTeams TeamResultTotal { get; set; }
    }
}
