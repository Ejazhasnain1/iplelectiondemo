﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IPLElectionDemo.Entities.Models.DBModels
{
    public class Users
    {
        [Key]
        public int UserId { get; set; }
        [StringLength(30)]
        public string FirstName { get; set; }
        [StringLength(30)]
        public string MiddleName { get; set; }
        [StringLength(30)]
        public string LastName { get; set; }
        [StringLength(60)]
        public string UserName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Mobile { get; set; }
        public string Gender { get; set; }
        [EmailAddress]
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public bool? IsPlayer { get; set; }

        [ForeignKey("UserRoleDetails")]
        public int RoleId { get; set; }
        public UserRoles UserRoleDetails { get; set; }
    }
}
