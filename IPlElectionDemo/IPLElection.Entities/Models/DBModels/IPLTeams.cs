﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IPLElectionDemo.Entities.Models.DBModels
{
    public class IPLTeams
    {
        [Key]
        public int TeamId { get; set; }
        public string TeamName { get; set; }
    }
}
