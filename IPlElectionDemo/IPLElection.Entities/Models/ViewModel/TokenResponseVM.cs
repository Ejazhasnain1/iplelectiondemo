﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IPLElectionDemo.Entities.Models.ViewModel
{
    public class TokenResponseVM
    {
        public string Token { get; set; }
        public DateTime Expires { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public int UserId { get; set; }
        public string RoleName { get; set; }
        public int? RoleId { get; set; }
        public string MiddleName { get; set; }
    }
}
