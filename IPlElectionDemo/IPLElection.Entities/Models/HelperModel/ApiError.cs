﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IPLElectionDemo.Entities.Models.HelperModel
{
    public class ApiError
    {
        public ApiError()
        {
            Errors = new List<string>();
        }
        public string Id { get; set; }
        public short Status { get; set; }
        public string Message { get; set; }
        public string Detail { get; set; }
        public IEnumerable<string> Errors { get; set; }
        public object Data { get; set; }
    }
}
